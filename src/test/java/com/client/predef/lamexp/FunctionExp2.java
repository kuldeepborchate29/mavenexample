package com.client.predef.lamexp;

import java.util.function.Function;

public class FunctionExp2 {

	public static void main(String[] args) {
		Function<Integer, Integer> square = no -> no * no;
		System.out.println(square.apply(2));
		System.out.println(square.apply(10));

		Function<String, Integer> lengthOf = name -> name.length();
		System.out.println(lengthOf.apply("Priyanka"));
		
		Function<String, String> upperCase=name->name.toUpperCase();
		System.out.println(upperCase.apply("Reshma"));

		
		
	}

}
