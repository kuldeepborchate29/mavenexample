package com.client.predef.lamexp;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

public class ConsumerExp1 {

	public static void main(String[] args) {
		Consumer<String> c = name -> System.out.println(name);
		c.accept("Kuldeep");

		BiConsumer<String, Integer> bc = (name, len) -> System.out.println(name.length() == len);
		bc.accept("KUL", 3);
		
		IntConsumer ic = name -> System.out.println(name);
		ic.accept(10);
	}

}
