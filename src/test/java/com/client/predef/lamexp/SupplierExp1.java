package com.client.predef.lamexp;

import java.util.function.Supplier;

public class SupplierExp1 {

	public static void main(String[] args) {

		Supplier<Double> s = () -> 3.12;

		System.out.println(s.get());

	}

}
