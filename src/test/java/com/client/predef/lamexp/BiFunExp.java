package com.client.predef.lamexp;

import java.util.function.BiFunction;

public class BiFunExp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BiFunction<String, String, Integer> conLength = (name1, name2) -> name1.concat(name2).length();

		System.out.println(conLength.apply("Mayur", "Sangram"));
	}

}
