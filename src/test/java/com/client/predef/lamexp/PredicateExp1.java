package com.client.predef.lamexp;

import java.util.function.Predicate;

public class PredicateExp1 {

	public static void main(String[] args) {
		Predicate<Integer> greterThan10 = x -> x > 10;

		System.out.println(greterThan10.test(500));
		System.out.println(greterThan10.test(5));

		Predicate<Integer> oddEven = x -> x % 2 == 0;
		System.out.println("Verify number is odd or Even -" + oddEven.test(20));
		System.out.println("Verify number is odd or Even -" + oddEven.test(11));

		String[] names = { "Katrina", "Mallika", "Sunny", "Kajal" };

		Predicate<String> strartWithK = name -> name.charAt(0) == 'K';

		for (String n : names) {
			if (strartWithK.test(n)) {
				System.out.println(n);
			}
		}

		System.out.println("Check no is greater than 10 and it's even =" + greterThan10.and(oddEven).test(20));
		System.out.println("Check no is greater than 10 or it's even =" + greterThan10.or(oddEven).test(10));
		
		System.out.println("Check no is less then 10 - "+greterThan10.negate().test(2));
	}
}