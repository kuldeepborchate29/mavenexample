package com.client.predef.lamexp;

import java.util.function.Predicate;

class User {

	String userName;
	String pwd;

	public User(String userName, String pwd) {
		this.userName = userName;
		this.pwd = pwd;
	}
}

public class PredicateExp2 {

	public static void main(String[] args) {
		User u1 = new User("Mahesh", "Kat");
		User u2 = new User("Sangram", "Kaj");
		User u3 = new User("Mayur", "Sunny");

		Predicate<User> authCheck = user -> user.userName.equals("Mahesh") && user.pwd.equals("Kat");

		System.out.println(authCheck.test(u1));
		System.out.println(authCheck.test(u2));
		System.out.println(authCheck.test(u3));
	}
}