package com.client.predef.lamexp;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class BiPredicateExp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BiPredicate<Integer, Integer> addOddEven = (a, b) -> (a + b) % 2 == 0;

		System.out.println(addOddEven.test(10, 20));


	}

}
