package com.client.predef.lamexp;

import java.util.function.Function;

class Student {
	int marks;
	String grade;
	
	public Student(int marks) {
		// TODO Auto-generated constructor stub
		this.marks=marks;
	}
}

public class FunctionExp1 {

	public static void main(String[] args) {

		Function<Student, String> chekStatus = std -> {
			int m = std.marks;
			String g;

			if (m >= 80) {
				g = "A";
			} else if (m >= 60) {
				g = "B";
			} else {
				g = "Failed";
			}
			return g;
		};
		
		System.out.println(chekStatus.apply(new Student(60)));
	}

}
