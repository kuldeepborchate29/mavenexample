package com.client.predef.lamexp;

import java.util.function.Function;

public class FunctionExp3 {

	public static void main(String[] args) {

		Function<Integer, Integer> square = no -> no * no;
		Function<Integer, Integer> cube = no -> no * no * no;

		System.out.println(square.andThen(cube).apply(2));

		System.out.println(square.andThen(cube).andThen(square).apply(2));

	}

}
