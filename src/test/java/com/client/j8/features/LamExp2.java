package com.client.j8.features;

interface A2 {
	public abstract void add(int a, int b);
}

public class LamExp2 {

	public static void main(String[] args) {
		A2 l2 = (a, b) -> System.out.println(a + b);
		l2.add(10, 20);
		l2.add(10, 20);
		l2.add(10, 40);
		l2.add(10, 20);
		l2.add(10, 20);
	}
}
