package com.client.j8.features;

interface Interf2 {
	void sub(int a, int b);
}

public class LamdaExp3 {

	public static void main(String[] args) {
		Interf2 d = (a, b) -> System.out.println(a - b);
		d.sub(10, 20);
		d.sub(10, 30);
	}
}