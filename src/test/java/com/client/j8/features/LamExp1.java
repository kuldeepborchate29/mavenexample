package com.client.j8.features;

interface A1 {
	public abstract void printMsg();
}

public class LamExp1 {

	public static void main(String[] args) {

		A1 l1 = () -> System.out.println("Hello world...!");

		l1.printMsg();
	}
}
