package com.client.j8.features;

interface Interf3 {
	int square(int a);
}

class Demo implements Interf3 {

	@Override
	public int square(int a) {
		return a * a;
	}
}

public class LamdaExp2 {

	public static void main(String[] args) {
		Interf3 d = (int a) -> {
			return a * a;
		};
		Interf3 d1 = a -> a * a;

		System.out.println("Sqaure = " + d.square(2));
		System.out.println("Sqaure = " + d.square(5));
		System.out.println("Sqaure = " + d.square(10));

		System.out.println("Sqaure = " + d1.square(2));
		System.out.println("Sqaure = " + d1.square(5));
		System.out.println("Sqaure = " + d1.square(10));
	}
}
