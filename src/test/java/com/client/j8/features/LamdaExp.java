package com.client.j8.features;

interface Interf {
	void m1();
}

public class LamdaExp {

	public static void main(String[] args) {
//		Demo d= new Demo();
		Interf d = () -> System.out.println("m1 implementation");

		d.m1();
		d.m1();
		d.m1();
		d.m1();
		d.m1();
		d.m1();
	}
}
