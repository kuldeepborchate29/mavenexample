package com.client.j8.features;

interface A3 {
	public abstract int square(int n);
}

public class LamExp3{
	
	public static void main(String[] args) {
		A3 l3 = n -> n * n;
		
		System.out.println(l3.square(2));
		System.out.println(l3.square(4));
		System.out.println(l3.square(5));
	}

}
