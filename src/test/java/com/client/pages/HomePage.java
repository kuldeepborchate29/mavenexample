package com.client.pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.client.locators.HomePageLocators;

public class HomePage {

	HomePageLocators hpl = null;

	public HomePage(WebDriver driver) {
		hpl = new HomePageLocators(driver);
	}

	public void clickOnbnwRadioButton() {
		hpl.bmwRadioButton.click();
		Assert.assertTrue(true, "I have failed this TCs to verify");
	}

	public void xyzOperationPerform() {

	}
}
