package com.client.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class SupportPage {

	WebDriver driver;

	public SupportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "name")
	WebElement nameTextBox;

	@FindBy(how = How.XPATH, using = "//a[text()='SUPPORT']")
	WebElement supportPageNavigate;

	public void clickOnSupportMenu() {
		supportPageNavigate.click();
		Assert.assertTrue(true, "I have failed this TCs to verify");
	}

	public void verifyNameTextBoxIsDisplayed() throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertTrue(nameTextBox.isDisplayed(), "i have passed this TCs");
	}

	public void verifySupportPageIsOpened() {
		clickOnSupportMenu();
		Assert.assertTrue(driver.getCurrentUrl().equals("www.xyz.com"));
	}
}
