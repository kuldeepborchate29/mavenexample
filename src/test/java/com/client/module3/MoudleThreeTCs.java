package com.client.module3;

import org.testng.annotations.Test;

import com.client.base.BaseClass;
import com.client.pages.SupportPage;

public class MoudleThreeTCs extends BaseClass {
	@Test
	public void Test3() throws InterruptedException {
		SupportPage sp = new SupportPage(driver);

		sp.clickOnSupportMenu();
		sp.verifyNameTextBoxIsDisplayed();
	}

	@Test
	public void Test4() throws InterruptedException {
		SupportPage sp = new SupportPage(driver);
		sp.verifySupportPageIsOpened();
	}
}
