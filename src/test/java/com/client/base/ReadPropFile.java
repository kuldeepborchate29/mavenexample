package com.client.base;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadPropFile {
	public Properties p = null;

	public Properties getPropObject() throws IOException {
//		FileReader reader = new FileReader(System.getProperty("user.dir")+"\\src\\com\\testng\\features\\Config.properties");
		FileReader reader = new FileReader(System.getProperty("user.dir")+"\\resources\\Config.properties");
		p = new Properties();
		p.load(reader);
		return p;
	}
}
