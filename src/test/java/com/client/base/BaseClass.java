package com.client.base;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseClass {

	public Properties p = null;
	protected WebDriver driver = null;

	public void launchBrowser() {

		String browser = p.getProperty("browser.name");

		switch (browser.toLowerCase()) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", p.getProperty("driver.chrome.path"));
			driver = new ChromeDriver();
			break;

		case "firefox":
			System.setProperty("webdriver.gecko.driver", "");
			driver = new FirefoxDriver();
			break;

		case "ie":
			System.setProperty("webdriver.ie.driver", p.getProperty("driver.ie.path"));
			driver = new InternetExplorerDriver();
			break;

		case "safari":
			System.setProperty("webdriver.safari.driver", "");
			driver = new SafariDriver();
			break;

		default:
			Assert.assertTrue(false, "No browser found name " + browser);
		}
		driver.manage().window().maximize();
	}

	public void hitUrl() {

		switch (p.getProperty("execution.env")) {
		case "SIT":
			driver.get(p.getProperty("url.sit"));
			break;
		case "SIT2":
			driver.get(p.getProperty("url.sit2"));
			break;
		case "UAT":
			driver.get(p.getProperty("url.uat"));
			break;
		default:
			Assert.assertTrue(false, "Invalid environment");
		}
	}

	@BeforeClass
	public void beforeClass() {
		try {
			p = new ReadPropFile().getPropObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		this.launchBrowser();
		this.hitUrl();
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}
}
