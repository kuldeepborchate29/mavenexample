package com.client.locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePageLocators {
	WebDriver driver;

	public HomePageLocators(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='name' and @type='text']") // findElement(By.xpath(""));
	WebElement textBox; // WebElement ele =

	@FindBy(how = How.ID, using = "bmwradio")
	public WebElement bmwRadioButton;

	@FindBy(how = How.XPATH, using = "//xyz")
	public WebElement hondaRadioButton;

}
