import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadTextFile {

	public static void main(String[] args) throws IOException {
		File file = new File("C:\\Users\\kulde\\OneDrive\\Desktop\\Batch.txt");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

//		System.out.println(br.readLine());
//		System.out.println(br.readLine());

		String line;

		while ((line = br.readLine()) != null) {
//			System.out.println(line);
			if(line.startsWith("Acc Details")) {
				String accNO=line.substring(12, 12+10);
				System.out.println("Account number - "+accNO);
				String accName=line.substring(22, 22+18);
				System.out.println("Account Name -"+accName);
			}
			if(line.startsWith("Pay Details")) {
				String payMethod=line.substring(12, 12+4);
				System.out.println("Payment method - "+payMethod);
			}
		}
	}

}
